import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  apptitle = 'Thetford';

  constructor() { }

  ngOnInit() {
    console.log('AppComponent geladen');
    window.scrollTo(0, 0);
  }

}
